import { css, Global, ThemeProvider } from "@emotion/react";
import { theme } from "./shared/theme/theme.ts";
import { BrowserRouter } from "react-router-dom";
import { AppRouting } from "./shared/routes/routes.tsx";

function App() {
  return (
    <ThemeProvider theme={theme}>
      <BrowserRouter>
        <Global
          styles={css`
            body {
              background: ${theme.colors.background};
              margin: 0;
              padding: 0;
              min-height: 100vh;
              max-width: 100vw;

              p {
                margin: 0;
              }
            }
          `}
        />
        <AppRouting />
      </BrowserRouter>
    </ThemeProvider>
  );
}

export default App;
