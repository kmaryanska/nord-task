import { api } from "src/shared/api/api.ts";
import { LoginRequest, LoginResponse } from "./dtos/login.dtos.ts";
import { authSlice } from "../store/auth.slice.ts";

export const authApi = api.injectEndpoints({
  endpoints: (builder) => ({
    login: builder.query<LoginResponse, LoginRequest>({
      extraOptions: { ignoreRefreshToken: true },
      query: (body) => ({
        url: "/tokens",
        method: "POST",
        body,
      }),
      onQueryStarted: async (_args, { dispatch, queryFulfilled }) => {
        try {
          const { data } = await queryFulfilled;
          document.cookie = `token=${data.token}`;
          dispatch(
            authSlice.actions.setUser({
              name: _args.username,
            }),
          );
          dispatch(authSlice.actions.setToken(data.token));
        } catch (error) {
          console.error(error);
        }
      },
    }),
  }),
});
export const { useLazyLoginQuery } = authApi;
