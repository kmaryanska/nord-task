import { http, HttpResponse } from "msw";
import { LoginRequest } from "../dtos/login.dtos.ts";

export const loginHandler = http.post(
  `https://test-api-url/v1/tokens`,
  async ({ request }) => {
    const { username, password } = (await request.json()) as LoginRequest;

    if (username === "tesonet" && password === "partyanimal") {
      return HttpResponse.json(
        {
          token: "test token",
        },
        { status: 200 },
      );
    }

    return HttpResponse.json({}, { status: 401 });
  },
);
