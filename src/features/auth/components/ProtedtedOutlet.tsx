import { Navigate, Outlet } from "react-router-dom";
import { LOGIN_PATHS } from "src/shared/routes/paths.tsx";
import { useCookie } from "src/shared/hooks/useCookie.ts";

export const ProtectedOutlet = () => {
  const [token] = useCookie("token");

  if (token) {
    return <Outlet />;
  }

  return <Navigate to={LOGIN_PATHS.Login} replace />;
};
