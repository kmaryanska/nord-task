import { Controller, SubmitHandler, useForm } from "react-hook-form";
import { zodResolver } from "@hookform/resolvers/zod";
import * as z from "zod";
import { useNavigate } from "react-router-dom";

import { useLazyLoginQuery } from "../../../../api/auth.api.ts";
import { APP_PATHS } from "../../../../../../shared/routes/paths.tsx";
import * as C from "../../../../../../shared/components";
import {
  passwordValidation,
  userNameValidation,
} from "../../../../../../shared/validation/validation.ts";

import * as S from "./styles.ts";
import { mapValidationToMessage } from "../../../../../../shared/validation/validation.logic.ts";

const schema = z.object({
  username: userNameValidation,
  password: passwordValidation,
});

type LoginFormValues = z.infer<typeof schema>;

export const LoginForm = () => {
  const {
    control,
    handleSubmit,
    formState: { errors },
  } = useForm<LoginFormValues>({
    mode: "all",
    defaultValues: {
      username: "",
      password: "",
    },
    resolver: zodResolver(schema),
  });

  const [login, { isLoading, isError }] = useLazyLoginQuery();
  const navigate = useNavigate();
  const onSubmit: SubmitHandler<LoginFormValues> = (data) => {
    login(data)
      .unwrap()
      .then(() => {
        navigate(APP_PATHS.Servers);
      })
      .catch((error) => {
        console.error("Validation error:", error);
      });
  };

  return (
    <S.LoginFormContainer>
      <S.LoginFormWrapper>
        <S.LoginFormStyled onSubmit={handleSubmit(onSubmit)}>
          <S.LoginFormTitle>Login</S.LoginFormTitle>

          <Controller
            name="username"
            render={({ field }) => (
              <C.InputField
                label="Username"
                type="text"
                placeholder="Type username"
                error={!!errors.username?.message}
                helperText={mapValidationToMessage(errors.username?.message)}
                {...field}
              />
            )}
            control={control}
          />

          <Controller
            name="password"
            render={({ field }) => (
              <C.InputField
                label="Password"
                type="password"
                placeholder="Type password"
                error={!!errors.password?.message}
                helperText={mapValidationToMessage(errors.password?.message)}
                {...field}
              />
            )}
            control={control}
          />

          <C.Button type="submit" variant="primary" isLoading={isLoading}>
            Login
          </C.Button>
          {isError && (
            <C.ErrorBanner>
              Please provide valid name and password
            </C.ErrorBanner>
          )}
        </S.LoginFormStyled>
      </S.LoginFormWrapper>
    </S.LoginFormContainer>
  );
};
