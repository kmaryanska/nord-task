import styled from "@emotion/styled";
import { theme } from "../../../../../../shared/theme/theme.ts";

export const LoginFormContainer = styled.div`
  width: 100%;
  display: flex;
  justify-content: center;
  align-items: center;
`;

export const LoginFormTitle = styled.p`
  font-size: 24px;
  font-weight: 600;
  margin-bottom: 54px;
  text-align: center;
  color: ${() => theme.colors.primary};
`;

export const LoginFormWrapper = styled.div`
  display: flex;
  flex-direction: column;
  justify-content: center;
  width: 100%;
  height: 500px;
  border: 1px solid #ccc;
  padding: 20px;
  margin: 0 20px;
  border-radius: 5px;
  background-color: #ffffff;

  @media (min-width: 768px) {
    width: 500px;
  }
`;

export const LoginFormStyled = styled.form`
  display: flex;
  flex-direction: column;
`;
