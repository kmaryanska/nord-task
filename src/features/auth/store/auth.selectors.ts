import { StoreState } from "src/shared/store/store.ts";

export const selectAuthentication = (state: StoreState) => state.auth;
export const selectUser = (state: StoreState) =>
  selectAuthentication(state)?.user;

export const selectToken = (state: StoreState) =>
  selectAuthentication(state)?.token;
