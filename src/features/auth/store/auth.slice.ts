import { createSlice, PayloadAction } from "@reduxjs/toolkit";

import { User } from "./auth.types";

type AuthState = {
  user?: User;
  token?: string;
};

const initialState: AuthState = {
  user: undefined,
  token: undefined,
};

export const authSlice = createSlice({
  name: "auth",
  initialState,
  reducers: {
    setUser: (state, action: PayloadAction<User | undefined>) => {
      state.user = action.payload;
    },
    setToken: (state, action: PayloadAction<string | undefined>) => {
      state.token = action.payload;
    },
  },
});
