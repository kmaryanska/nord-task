export type GetServersRequest = null;

export type Server = {
  distance: number;
  name: string;
};

export type GetServersResponse = Server[];
