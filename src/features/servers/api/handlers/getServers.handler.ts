import { http, HttpResponse } from "msw";
import { GetServersResponse } from "../dtos/servers.dtos.ts";

export const getServersHandler_success = http.get(
  `https://test-api-url/v1/servers`,
  () => {
    return HttpResponse.json(
      [
        { name: "test-server", distance: 100 },
        { name: "test-server-2", distance: 200 },
        {
          name: "test-server-3",
          distance: 300,
        },
      ] satisfies GetServersResponse,
      { status: 200 },
    );
  },
);
