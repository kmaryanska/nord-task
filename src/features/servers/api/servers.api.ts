import { api } from "src/shared/api/api.ts";
import { GetServersRequest, GetServersResponse } from "./dtos/servers.dtos.ts";

export const serversApi = api.injectEndpoints({
  endpoints: (builder) => ({
    getServers: builder.query<GetServersResponse, GetServersRequest>({
      query: () => "/servers",
      providesTags: ["SERVERS"],
    }),
  }),
});
export const { useGetServersQuery } = serversApi;
