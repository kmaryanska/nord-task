import * as C from "src/shared/components";

import { useGetServersQuery } from "../api/servers.api.ts";

export const ServersList = () => {
  const { data: servers, isLoading, isError } = useGetServersQuery(null);
  return (
    <C.Table
      data={servers}
      isError={isError}
      loading={isLoading}
      emptyMessage="no servers available"
      sortBy={["distance", "name"]}
    />
  );
};
