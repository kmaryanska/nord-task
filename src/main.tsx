import "src/assets/fonts/font-definitions.css";
import { Provider } from "react-redux";
import React from "react";
import ReactDOM from "react-dom/client";

import App from "./App.tsx";
import { store } from "./shared/store/store.ts";

ReactDOM.createRoot(document.getElementById("root")!).render(
  <Provider store={store}>
    <React.StrictMode>
      <App />
    </React.StrictMode>
  </Provider>,
);
