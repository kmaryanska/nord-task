import { loginHandler } from "../features/auth/api/handlers/login.handler.ts";

export const handlers = [loginHandler];
