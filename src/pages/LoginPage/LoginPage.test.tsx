import "@testing-library/jest-dom";
import { Mock } from "vitest";
import { screen } from "@testing-library/react";
import { generatePath, MemoryRouter } from "react-router-dom";

import { LOGIN_PATHS } from "src/shared/routes/paths.tsx";
import { render, userEvent } from "src/test/tests.utils.tsx";
import { AppRouting } from "src/shared/routes/routes.tsx";
import { User } from "src/features/auth/store/auth.types.ts";
import { useCookie } from "src/shared/hooks/useCookie.ts";
import { loginHandler } from "src/features/auth/api/handlers/login.handler.ts";
import { server } from "src/msw/worker.ts";

vi.mock("src/shared/hooks/useCookie", () => ({
  useCookie: vi.fn(),
}));

const renderLoginPage = (user: User | undefined) => {
  render(
    <MemoryRouter initialEntries={[generatePath(LOGIN_PATHS.Login)]}>
      <AppRouting />
    </MemoryRouter>,
    {
      preloadedState: {
        auth: {
          user,
        },
      },
    },
  );
};

describe(`Given user is on ${LOGIN_PATHS.Login} page`, () => {
  describe("And user is already authenticated", () => {
    beforeEach(() => {
      (useCookie as Mock).mockReturnValue(["mockToken", vi.fn(), vi.fn()]);
      renderLoginPage({ name: "Test User" });
    });

    test("Then user is redirected to Servers Page", async () => {
      expect(await screen.findByText("Servers Page")).toBeInTheDocument();
    });
  });

  describe("And user is not authenticated", () => {
    beforeEach(() => {
      (useCookie as Mock).mockReturnValue([undefined, vi.fn(), vi.fn()]);
      server.use(loginHandler);
      renderLoginPage(undefined);
    });

    test("Then Login Page is displayed", () => {
      expect(screen.getByText("Welcome to Login Page!")).toBeInTheDocument();
      expect(
        screen.getByText(
          "Please provide user name and password to use the Server App!",
        ),
      ).toBeInTheDocument();
    });

    test("And Login Form is visible", () => {
      const usernameInput = screen.getByLabelText("Username");
      const passwordInput = screen.getByLabelText("Password");
      const loginButton = screen.getByRole("button", { name: "Login" });

      expect(loginButton).toBeInTheDocument();
      expect(usernameInput).toBeInTheDocument();
      expect(passwordInput).toBeInTheDocument();
    });

    test("And user see information about required fields after clicking login button", async () => {
      const loginButton = screen.getByRole("button", { name: "Login" });

      await userEvent.click(loginButton);

      expect(await screen.findAllByText("This field is required")).toHaveLength(
        2,
      );
    });

    test("And user see error message after submitting the form with incorrect credentials", async () => {
      const usernameInput = screen.getByLabelText("Username");
      const passwordInput = screen.getByLabelText("Password");
      const loginButton = screen.getByRole("button", { name: "Login" });

      await userEvent.type(usernameInput, "wrong name");
      await userEvent.type(passwordInput, "wrong password");
      await userEvent.click(loginButton);

      expect(
        await screen.findByText("Please provide valid name and password"),
      ).toBeInTheDocument();
    });

    test("And user is redirected to Servers Page after submitting the form with valid credentials", async () => {
      (useCookie as Mock).mockReturnValue(["mockToken", vi.fn(), vi.fn()]);

      const usernameInput = screen.getByLabelText("Username");
      const passwordInput = screen.getByLabelText("Password");
      const loginButton = screen.getByRole("button", {
        name: "Login",
      });

      await userEvent.type(usernameInput, "tesonet");
      await userEvent.type(passwordInput, "partyanimal");
      await userEvent.click(loginButton);

      expect(await screen.findByText("Servers Page")).toBeInTheDocument();
    });
  });
});
