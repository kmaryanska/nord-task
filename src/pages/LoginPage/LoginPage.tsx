import { useEffect } from "react";
import { useNavigate } from "react-router-dom";

import { APP_PATHS } from "src/shared/routes/paths.tsx";
import { LoginForm } from "../../features/auth/components/login/components/LoginForm/LoginForm.tsx";
import * as C from "src/shared/components";

import { useCookie } from "src/shared/hooks/useCookie.ts";

export const LoginPage = () => {
  const [token] = useCookie("token");
  const navigate = useNavigate();

  useEffect(() => {
    if (token) {
      navigate(APP_PATHS.Servers);
    }
  }, [token]);

  return (
    <C.CommonPageContainer
      title="Welcome to Login Page!"
      subtitle="Please provide user name and password to use the Server App!"
    >
      <LoginForm />
    </C.CommonPageContainer>
  );
};
