import "@testing-library/jest-dom";
import { Mock } from "vitest";
import { screen } from "@testing-library/react";
import { generatePath, MemoryRouter } from "react-router-dom";

import { APP_PATHS } from "src/shared/routes/paths.tsx";
import { render } from "src/test/tests.utils.tsx";
import { AppRouting } from "src/shared/routes/routes.tsx";
import { User } from "src/features/auth/store/auth.types.ts";
import { useCookie } from "src/shared/hooks/useCookie.ts";

vi.mock("src/shared/hooks/useCookie", () => ({
  useCookie: vi.fn(),
}));
const renderServersPage = (user: User | undefined) => {
  render(
    <MemoryRouter initialEntries={[generatePath(APP_PATHS.Servers)]}>
      <AppRouting />
    </MemoryRouter>,
    {
      preloadedState: {
        auth: {
          user,
        },
      },
    },
  );
};
describe(`Given user is on ${APP_PATHS.Servers} page`, () => {
  describe("When user is already authenticated", () => {
    beforeEach(() => {
      (useCookie as Mock).mockReturnValue(["mockToken", vi.fn(), vi.fn()]);
      renderServersPage({ name: "Test User" });
    });

    test("Then user see Servers Page title", async () => {
      expect(await screen.findByText("Servers Page")).toBeInTheDocument();
    });
  });
});
