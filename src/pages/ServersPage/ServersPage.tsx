import { ServersList } from "src/features/servers/components/ServerList.tsx";
import * as C from "src/shared/components";

export const ServersPage = () => {
  return (
    <C.CommonPageContainer title="Servers Page">
      <ServersList />
    </C.CommonPageContainer>
  );
};
