import { createApi } from "@reduxjs/toolkit/query/react";
import { baseQuery } from "./base-query.ts";

export const api = createApi({
  reducerPath: "api",
  baseQuery: baseQuery,
  endpoints: () => ({}),
  tagTypes: ["SERVERS"],
});
