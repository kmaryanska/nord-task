import { fetchBaseQuery } from "@reduxjs/toolkit/query";
import { getCookie } from "../utils.ts";

const baseQuery = fetchBaseQuery({
  baseUrl: `https://playground.tesonet.lt/v1`,
  prepareHeaders: (headers) => {
    const token = getCookie("token");

    if (token) {
      headers.set("authorization", `Bearer ${token}`);
      return headers;
    }

    return headers;
  },
});

export { baseQuery };
