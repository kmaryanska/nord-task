import { ButtonHTMLAttributes, ReactNode } from "react";
import styled from "@emotion/styled";

import { theme } from "src/shared/theme/theme.ts";

type ButtonProps = {
  variant: "primary" | "secondary";
  children: ReactNode;
  isLoading?: boolean;
  onClick?: () => void;
} & ButtonHTMLAttributes<HTMLButtonElement>;

const StyledButton = styled.button<ButtonProps>`
  background-color: ${({ variant }) =>
    variant ? theme.colors.primary : theme.colors.secondary};
  padding: 10px 20px;
  color: white;
  border: none;
  border-radius: 3px;
  cursor: pointer;

  &:hover {
    background-color: ${() => theme.colors.hover};
  }
`;

export const Button = ({
  variant,
  children,
  isLoading,
  onClick,
  ...rest
}: ButtonProps) => {
  return (
    <StyledButton variant={variant} onClick={onClick} {...rest}>
      {!isLoading ? children : <>loading</>}
    </StyledButton>
  );
};
