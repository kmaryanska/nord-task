import { InputHTMLAttributes } from "react";
import styled from "@emotion/styled";
import { theme } from "src/shared/theme/theme.ts";

type InputFieldProps = {
  name: string;
  label: string;
  type: string;
  placeholder: string;
  error?: boolean;
  helperText?: string;
} & InputHTMLAttributes<HTMLInputElement>;

const InputLabel = styled.label`
  margin-bottom: 5px;
`;

const StyledInputField = styled.input<Partial<InputFieldProps>>`
  padding: 8px;
  border-radius: 3px;
  border: 1px solid
    ${({ error }) => (error ? theme.colors.error.main : theme.colors.border)};
  height: 40px;
`;

const StyledWrapper = styled.div`
  display: flex;
  flex-direction: column;
  margin-bottom: 28px;
`;

const ErrorMessage = styled.p`
  margin-top: 3px;
  font-size: 12px;
  color: ${() => theme.colors.error.main};
`;

export const InputField = ({
  label,
  type,
  placeholder,
  error,
  helperText,
  ...rest
}: InputFieldProps) => {
  return (
    <StyledWrapper>
      <InputLabel htmlFor={label}>{label}</InputLabel>
      <StyledInputField
        id={label}
        type={type}
        placeholder={placeholder}
        error={error}
        {...rest}
      />
      {helperText && <ErrorMessage>{helperText}</ErrorMessage>}
    </StyledWrapper>
  );
};
