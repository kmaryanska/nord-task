import { ReactNode } from "react";
import styled from "@emotion/styled";

import { theme } from "src/shared/theme/theme.ts";

type ErrorBannerProps = {
  children: ReactNode;
};

const StyledDiv = styled.div`
  background-color: ${() => theme.colors.error.light};
  padding: 10px 20px;
  color: white;
  border-radius: 3px;
  margin-top: 10px;
`;

const StyledText = styled.p`
  font-size: 14px;
  font-weight: 400;
  margin: 0;
  color: ${() => theme.colors.error.main};
`;

export const ErrorBanner = ({ children }: ErrorBannerProps) => {
  return (
    <StyledDiv>
      <StyledText>{children}</StyledText>
    </StyledDiv>
  );
};
