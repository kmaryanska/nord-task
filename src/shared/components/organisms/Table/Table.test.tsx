import "@testing-library/jest-dom";
import { screen } from "@testing-library/react";
import { Table } from "./Table";
import { render, userEvent } from "src/test/tests.utils.tsx";

describe("Table component", () => {
  const mockData = [
    { id: 1, name: "Alice", age: 30 },
    { id: 2, name: "Bob", age: 25 },
    { id: 3, name: "Charlie", age: 35 },
  ];

  test("renders loading state when loading is true", () => {
    render(
      <Table loading={true} isError={false} emptyMessage="" sortBy={[]} />,
    );
    expect(screen.getByText("Loading...")).toBeInTheDocument();
  });

  test("renders error state when error is present", () => {
    render(
      <Table loading={false} isError={true} emptyMessage="" sortBy={[]} />,
    );
    expect(screen.getByText("Error: Something went wrong")).toBeInTheDocument();
  });

  test("renders empty message when data is empty", () => {
    render(
      <Table
        data={[]}
        loading={false}
        isError={false}
        emptyMessage="No data"
        sortBy={[]}
      />,
    );
    expect(screen.getByText("No data")).toBeInTheDocument();
  });

  test("renders table headers and data correctly", () => {
    render(
      <Table
        data={mockData}
        loading={false}
        isError={false}
        emptyMessage=""
        sortBy={["id", "name", "age"]}
      />,
    );

    // Check table headers
    expect(screen.getByText("id")).toBeInTheDocument();
    expect(screen.getByText("name")).toBeInTheDocument();
    expect(screen.getByText("age")).toBeInTheDocument();

    // Check table data
    expect(screen.getByText("1")).toBeInTheDocument();
    expect(screen.getByText("Alice")).toBeInTheDocument();
    expect(screen.getByText("30")).toBeInTheDocument();
    expect(screen.getByText("2")).toBeInTheDocument();
    expect(screen.getByText("Bob")).toBeInTheDocument();
    expect(screen.getByText("25")).toBeInTheDocument();
    expect(screen.getByText("3")).toBeInTheDocument();
    expect(screen.getByText("Charlie")).toBeInTheDocument();
    expect(screen.getByText("35")).toBeInTheDocument();
  });

  test("sorts table data when header is clicked", async () => {
    render(
      <Table
        data={mockData}
        loading={false}
        isError={false}
        emptyMessage=""
        sortBy={["id", "name", "age"]}
      />,
    );

    // Click on the "name" header to sort alphabetically
    await userEvent.click(screen.getByText("name"));

    // Check if the data is sorted correctly
    const nameColumn = screen.getAllByText(/Alice|Bob|Charlie/);
    expect(nameColumn[0]).toHaveTextContent("Alice");
    expect(nameColumn[1]).toHaveTextContent("Bob");
    expect(nameColumn[2]).toHaveTextContent("Charlie");

    // Click on the "name" header again to toggle sorting direction
    await userEvent.click(screen.getByText("name"));

    // Check if the data is sorted in reverse order
    const nameColumnReversed = screen.getAllByText(/Alice|Bob|Charlie/);
    expect(nameColumnReversed[0]).toHaveTextContent("Charlie");
    expect(nameColumnReversed[1]).toHaveTextContent("Bob");
    expect(nameColumnReversed[2]).toHaveTextContent("Alice");
  });
});
