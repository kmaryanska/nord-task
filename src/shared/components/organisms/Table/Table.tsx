import { useEffect, useState } from "react";

import * as S from "./styles";

export type TableRow<T> = {
  [key in keyof T]: string;
};

export type FetchResponse<T> = T[];

type TableProps<T> = {
  data?: FetchResponse<T>;
  loading: boolean;
  isError: boolean;
  emptyMessage: string;
  sortBy: (keyof T)[];
};

export const Table = <T extends Record<string, any>>({
  data,
  loading,
  isError,
  emptyMessage,
  sortBy,
}: TableProps<T>) => {
  const [sortedData, setSortedData] = useState<TableRow<T>[] | undefined>(
    undefined,
  );

  const [sortConfig, setSortConfig] = useState<
    { key: keyof T; sortOrder: "asc" | "desc" } | undefined
  >();

  useEffect(() => {
    if (data) {
      setSortedData([...data]);
    }
  }, [data]);

  const sortData = (key: string) => {
    let sortOrder: "asc" | "desc" = "asc";

    if (sortConfig && sortConfig.key === key) {
      sortOrder = sortConfig.sortOrder === "asc" ? "desc" : "asc";
    }

    const newSortedData = [...sortedData!].sort((a, b) => {
      if (a[key] < b[key]) return sortOrder === "asc" ? -1 : 1;
      if (a[key] > b[key]) return sortOrder === "asc" ? 1 : -1;
      return 0;
    });

    setSortedData(newSortedData);
    setSortConfig({ key, sortOrder });
  };

  return (
    <S.TableContainer>
      {loading && <p>Loading...</p>}
      {isError && <p>Error: Something went wrong</p>}
      {!loading && !isError && sortedData?.length === 0 && (
        <p>{emptyMessage}</p>
      )}
      {!loading && !isError && sortedData && sortedData?.length > 0 && (
        <S.TableWrapper>
          <S.TableHead>
            <S.TableRow>
              {Object.keys(sortedData[0]).map((key) => (
                <S.TableHeaderCell key={key} onClick={() => sortData(key)}>
                  {key}
                  {Array.isArray(sortBy) && sortBy.includes(key) && (
                    <S.SortIcon
                      sortOrder={
                        sortConfig?.key === key
                          ? sortConfig.sortOrder
                          : undefined
                      }
                    />
                  )}
                </S.TableHeaderCell>
              ))}
            </S.TableRow>
          </S.TableHead>
          <S.TableBody>
            {sortedData.map((row, index) => (
              <S.TableRow key={index}>
                {Object.values(row).map((value, index) => (
                  <S.TableCell key={index}>{value}</S.TableCell>
                ))}
              </S.TableRow>
            ))}
          </S.TableBody>
        </S.TableWrapper>
      )}
    </S.TableContainer>
  );
};
