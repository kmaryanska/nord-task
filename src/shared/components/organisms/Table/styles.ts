import styled from "@emotion/styled";
import { theme } from "src/shared/theme/theme.ts";

export const TableContainer = styled.div`
  width: 100%;
  margin: 0 auto;

  @media (min-width: 768px) {
    width: 600px;
  }
`;

export const TableWrapper = styled.table`
  width: 100%;
  border-collapse: collapse;
  margin-bottom: 20px;
`;

export const TableHead = styled.thead``;

export const TableBody = styled.tbody``;

export const TableRow = styled.tr`
  height: 50px;
  background-color: white;
`;

export const TableHeaderCell = styled.th`
  padding: 10px;
  background-color: white;
  border-bottom: 2px solid;
  border-bottom-color: ${theme.colors.border};
  cursor: pointer;
`;

export const TableCell = styled.td`
  text-align: center;
  padding: 10px;
  border-bottom: 1px solid #ddd;
`;

export const SortIcon = styled.span<{ sortOrder?: string }>`
  margin-left: 5px;
  vertical-align: middle;
  ${({ sortOrder }) =>
    sortOrder === "asc"
      ? `
    &:after {
      content: '▲';
    }
  `
      : `
    &:after {
      content: '▼';
    }
  `}
`;
