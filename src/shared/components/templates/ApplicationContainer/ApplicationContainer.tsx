import { useDispatch } from "react-redux";
import { Outlet, useNavigate } from "react-router-dom";

import { useCookie } from "src/shared/hooks/useCookie.ts";
import * as C from "src/shared/components";
import { authSlice } from "src/features/auth/store/auth.slice.ts";

import * as S from "./styles";
import { LOGIN_PATHS } from "src/shared/routes/paths.tsx";

export const ApplicationContainer = () => {
  const [, , resetCookie] = useCookie("token");

  const dispatch = useDispatch();
  const navigate = useNavigate();
  const handleLogout = () => {
    resetCookie();
    dispatch(authSlice.actions.setUser(undefined));
    navigate(LOGIN_PATHS.Login);
  };

  return (
    <S.ApplicationWrapper>
      <S.Header>
        <C.Button variant="secondary" onClick={handleLogout}>
          Logout
        </C.Button>
      </S.Header>
      <Outlet />
    </S.ApplicationWrapper>
  );
};
