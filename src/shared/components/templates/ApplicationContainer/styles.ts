import styled from "@emotion/styled";

export const ApplicationWrapper = styled.div`
  height: 100vh;
  width: 100vw;
`;
export const Header = styled.div`
  position: relative;
  display: flex;
  height: 60px;
  justify-content: end;
  align-items: center;
  background-color: white;
  padding: 0 40px 0 40px;

  & > p {
    font-size: 18px;
  }
`;
