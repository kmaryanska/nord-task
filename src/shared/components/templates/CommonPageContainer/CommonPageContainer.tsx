import { ReactNode } from "react";

import * as S from "./styles";

type CommonPageWrapperProps = {
  title: string;
  subtitle?: string;
  children: ReactNode;
};
export const CommonPageContainer = ({
  title,
  subtitle,
  children,
}: CommonPageWrapperProps) => {
  return (
    <S.PageContainer>
      <S.TitleWrapper>
        <S.PageTitle>{title}</S.PageTitle>
        {subtitle && <S.Subtitle>{subtitle}</S.Subtitle>}
      </S.TitleWrapper>

      {children}
    </S.PageContainer>
  );
};
