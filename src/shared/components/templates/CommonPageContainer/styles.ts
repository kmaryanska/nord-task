import styled from "@emotion/styled";

export const PageContainer = styled.div`
  display: flex;
  flex-direction: column;
  justify-content: center;
  align-items: center;
`;
export const TitleWrapper = styled.div`
  display: flex;
  flex-direction: column;
  justify-content: center;
  align-items: center;
  margin-left: 20px;
  margin-right: 20px;
  margin-top: 48px;
  margin-bottom: 48px;
`;

export const PageTitle = styled.p`
  font-size: 24px;
  font-weight: 600;
  margin-bottom: 12px;
  text-align: center;
`;

export const Subtitle = styled.p`
  font-size: 18px;
  font-weight: 400;
  text-align: center;
`;
