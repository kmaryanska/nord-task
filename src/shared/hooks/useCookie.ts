import { useState } from "react";

export const useCookie = (cookieName: string) => {
  const getCookie = (name: string) => {
    const cookies = document.cookie.split(";");
    for (const cookie of cookies) {
      const [cookieName, cookieValue] = cookie.trim().split("=");
      if (cookieName === name) {
        return cookieValue;
      }
    }
    return null;
  };

  const [cookie, setCookie] = useState(getCookie(cookieName));

  const updateCookie = (value: string) => {
    document.cookie = `${cookieName}=${value}`;
    setCookie(value);
  };

  const resetCookie = () => {
    updateCookie("");
  };

  return [cookie, updateCookie, resetCookie] as const;
};
