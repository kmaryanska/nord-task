export enum APP_PATHS {
  Servers = "/servers",
  NotFound = "/not-found",
}

export enum LOGIN_PATHS {
  Login = "/login",
  Logout = "/logout",
  Unauthorized = "/unauthorized",
}
