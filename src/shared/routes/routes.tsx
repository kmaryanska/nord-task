import { Navigate, Route, Routes } from "react-router-dom";

import { ServersPage } from "src/pages/ServersPage/ServersPage.tsx";
import { LoginPage } from "src/pages/LoginPage/LoginPage.tsx";
import { ProtectedOutlet } from "src/features/auth/components/ProtedtedOutlet.tsx";

import { APP_PATHS, LOGIN_PATHS } from "./paths.tsx";
import { ApplicationContainer } from "../components";

export function AppRouting() {
  return (
    <Routes>
      <Route path={LOGIN_PATHS.Login} element={<LoginPage />} />
      <Route element={<ApplicationContainer />}>
        <Route element={<ProtectedOutlet />}>
          <Route
            path="/"
            element={<Navigate to={APP_PATHS.Servers} replace={true} />}
          />

          <Route path={APP_PATHS.Servers} element={<ServersPage />} />
        </Route>
      </Route>
      <Route path="*" element={<div>Not found</div>} />
    </Routes>
  );
}
