import { combineReducers } from "redux";

import { authSlice } from "src/features/auth/store/auth.slice.ts";

import { api } from "../api/api.ts";

const reducers = {
  [api.reducerPath]: api.reducer,
  [authSlice.name]: authSlice.reducer,
};

export default combineReducers<typeof reducers>(reducers);
