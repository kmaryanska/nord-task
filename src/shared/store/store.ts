import { configureStore } from "@reduxjs/toolkit";

import reducers from "./reducers";
import { api } from "../api/api.ts";

export const createStore = (preloadedState?: Partial<StoreState>) =>
  configureStore({
    reducer: reducers,
    middleware: (getDefaultMiddleware) =>
      getDefaultMiddleware().concat([api.middleware]),
    preloadedState,
  });

export const store = createStore();
export type StoreState = ReturnType<typeof reducers>;
