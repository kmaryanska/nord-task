import "src/assets/fonts/font-definitions.css";

export const theme = {
  colors: {
    primary: "#FF5F7B",
    hover: "#FF4F84",
    secondary: "#FF8463",
    background: "#f6f6f6",
    border: "#dddddd",
    error: {
      light: "#FEE1E1",
      main: "#EF4344",
      dark: "#B50203",
    },
  },
  breakpoints: [576, 768, 992, 1200],
};
