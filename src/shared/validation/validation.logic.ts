import { Validation } from "./validation.const";

export function mapValidationToMessage(
  validationKey?: string,
): string | undefined {
  if (validationKey && validationKey in Validation) {
    switch (validationKey) {
      case Validation.REQUIRED:
        return "This field is required";
      case Validation.MAX_LENGTH_100:
        return "Maximum length exceeded (100 characters)";
      // add more cases for other validation keys
      default:
        return "Unknown validation error.";
    }
  }
  return undefined;
}
