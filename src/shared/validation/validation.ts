import { z } from "zod";

import { Validation } from "./validation.const";

export const userNameValidation = z
  .string({
    required_error: Validation.REQUIRED,
  })
  .min(1, Validation.REQUIRED);
export const passwordValidation = z
  .string({
    required_error: Validation.REQUIRED,
  })
  .min(1, Validation.REQUIRED);
