import { render } from "@testing-library/react";
import userEvent from "@testing-library/user-event";

import React, { ReactNode } from "react";

import { Provider } from "react-redux";

import { MemoryRouter, MemoryRouterProps } from "react-router-dom";
import { createStore, StoreState } from "../shared/store/store.ts";

type RenderOptions = {
  preloadedState?: Partial<StoreState>;
  memoryRouterProps?: MemoryRouterProps;
};

const customRender = (
  ui: any,
  { preloadedState, ...renderOptions }: RenderOptions = {},
) => {
  const store = createStore({ ...preloadedState });

  const renderInMemoryRouter = (children: ReactNode) => (
    <MemoryRouter {...renderOptions.memoryRouterProps}>{children}</MemoryRouter>
  );

  const renderChildren = (children: ReactNode) => {
    if (renderOptions.memoryRouterProps) {
      return renderInMemoryRouter(children);
    }

    return children;
  };

  const Wrapper = ({ children }: Readonly<{ children: React.ReactNode }>) => (
    <Provider store={store}>{renderChildren(<>{children}</>)}</Provider>
  );

  return {
    store,
    ...render(ui, { wrapper: Wrapper, ...renderOptions }),
  };
};

export * from "@testing-library/react";

export { customRender as render, userEvent };
